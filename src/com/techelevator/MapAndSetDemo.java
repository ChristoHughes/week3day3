package com.techelevator;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class MapAndSetDemo {
	public static void main(String[] args) {
		
		// ############ SET ################
		
		System.out.println("Sets cannot contain duplicates\n");
		
		Set<String> students = new HashSet<String>();  // change HashSet to TreeSet or LinkedHashSet to see how the behavior changes
		
		students.add("Jim");
		System.out.println("Added Jim");
		students.add("Seth");
		System.out.println("Added Seth");
		students.add("Dan");
		System.out.println("Added Dan");
		students.add("Jim");  // this value will be ignored because it is duplicate
		System.out.println("Added Jim again\n");
		
		System.out.println("Set of students contains:");
		for(String name : students) { // note that the values are returned in a different order than they were added
			System.out.println(name+" : Hash = "+name.hashCode());
		}
		System.out.println("\nnote that the values are returned in a different order than they were added");
		
		// ############ MAP ################
		System.out.println("\n\n");
		
		Map<String, String> nameToZip = new HashMap<String, String>();  // Map is an interface and HashMap is a class that implements Map
		
		nameToZip.put("David", "44120");
		nameToZip.put("Dan", "44124");
		nameToZip.put("Elizabeth", "44012");
		
		System.out.println("David lives in "+nameToZip.get("David"));
		System.out.println("Dan lives in "+nameToZip.get("Dan"));
		System.out.println("Elizabeth lives in "+nameToZip.get("Elizabeth"));
		
		System.out.println("\n\nUsing a for loop:");
		Set<String> keys = nameToZip.keySet();		// returns a Set of all of the keys in the Map
		for(String name : keys) {
			System.out.println(name+" lives in "+nameToZip.get(name));
			
		}
		
		System.out.println("\n\nput 12345 for key David\n");
		nameToZip.put("David", "12345");  // The key "David" already exists, so this line will overwrite the existing value with the new value
		for(String name : keys) {
			System.out.println(name+" lives in "+nameToZip.get(name));
		}
		
		nameToZip.remove("David");	
		System.out.println("\n\nremoved David\n");
		for(String name : keys) {
			System.out.println(name+" lives in "+nameToZip.get(name));
		}
	}
}
